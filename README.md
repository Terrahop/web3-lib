# web3-lib

### **NOTE, if you are viewing this on github, you are looking at a mirror, original is on gitlab [here](https://gitlab.com/Terrahop/web3-lib) for issues, PR's etc.**

A library containing web3 methods for most of the functions of the [web 3 api](https://web3js.readthedocs.io/en/1.0/index.html) for easier implementation

## Overview

### Exposed Functionality

#### Account Related
* UnlockAccount
* CreateAccount
* CreateAccountWithPassword
* CreateAccountFromPrivateKey
* CheckBalance

#### Contract Related
* DeployContractFromUnlockedAccount
* DeployContractWithPrivateKey
* MethodCall
* MethodSend
* EstimateGas
* GetPastEvents
* CreateTransferTransaction

#### Transaction Related
* SignTransaction
* SendSignedTransaction
* SendTransaction
* GetTransactionReceipt

#### Utils and Helper Related
* isAddress
* randomHex
* ToHex
* ToBN
* HexToNumberString
* CorrectDecimals
* CalculateAmount

## How to Use
#### Note, the address used here are examples and need to be substited with your own
Basic server and account initalization.
```
const Server = require('./src/Server')

// Create a new Server Instance(RPC Connection)
// Defaults to 'https://rinkeby.infura.io'
let server = new Server({})

// Create Contract factory, allows for creating seperate contract instances
let contractFactory = server.newContractFactory()

// Create account instance for web3's 'account' related methods
let account = server.newAccount()

// Create an account with a password on the network
account.createAccountWithPassword('password')

// Unlock an account with a password for 600 seconds
account.unlockAccount('0x0d4350e46030afAF2F0E3fbDCA1C566Fb2c53b11', 'password', 600)
```
This library allows for the initalization of a contract by either reading directly from a .sol contract file or using a contracts generated ABI and Bytecode.

To read from a .sol file using createContractFromSol:
```
// Create Contract instance
// Providing the owner address(needs to be an unlock account), path to .sol file and the class name of the contract.

let contractInstance = contractFactory.createContractFromSol(
  '0x0d4350e46030afAF2F0E3fbDCA1C566Fb2c53b11',
  './src/contracts/Token.sol',
  'Token' // This needs to be the root classname of the contract
)
```

To read from a contracts ABI and Bytecode using createContract:
```
let contractInstance = contractFactory.createContract(
  '0x0d4350e46030afAF2F0E3fbDCA1C566Fb2c53b11',
  'Contract ByteCode,
  'Contract JSON ABI'
)
```

To deploy a contract from an unlocked account and get the total supply of tokens provided by the contract:
```
// Deploy our contract to the network and read the total supply of tokens provided by it.
// In order to get the total supply of tokens provided, there needs to be a 'totalSupply' function in the contract
// providing this logic
contractInstance.deployContractFromUnlockedAccount([contract arguments if any]).then(res => {
  console.log(`Deployed to ${res.options.address}\nGetting Total supply of tokens`)
  contractInstanceToDeploy.methodCall('totalSupply').then(res => console.log(`Got: ${res}`))
})
```

You can also create a contract instance for an already deployed contract if you provide a deployed address
```
let deployedContractInstance = contractFactory.createContractFromSol(
  '0x0d4350e46030afAF2F0E3fbDCA1C566Fb2c53b11',
  './src/contracts/Token.sol',
  'Token',
  '0x0d1470e62139afAF2F0E3fbDCA1C351Fb1c23b49' // The address of the deployed contract
)
```
## Contributors
***
* [@terrahop](https://github.com/terrahop)
* [@EternalDeiwos](https://github.com/EternalDeiwos)
