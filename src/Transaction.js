'use strict'

/**
 * Transaction
 * Contains functions for creating and sending transactions to the network
 * @class
 */
class Transaction {
  constructor (RPCInstance) {
    Object.defineProperty(this, 'web3', { value: RPCInstance, writable: true })
  }

  /**
 * Signs an Ethereum transaction with a given private key.
 *
 * @async
 * @function signTransaction
 * @param {object} tx The transaction object to send.
 * @param {string} tx.nonce (optional) The nonce to use when signing this transaction.
 * @param {string} tx.chainId (optional) The chain id to use when signing this transaction.
 * @param {string} tx.to (optional) The recevier of the transaction, can be empty when deploying a contract.
 * @param {string} tx.data (optional) The call data of the transaction, can be empty for simple value transfers.
 * @param {string} tx.value (optional) The value of the transaction in wei.
 * @param {string} tx.gasPrice (optional) The value of the transaction in wei.
 * @param {string} tx.gas (optional) The gas price set by this transaction.
 * @param {string} privateKey Private key for the account that needs to sign the transaction.
 * @returns {object} The signed data RLP encoded transaction, or if returnSignature is true the signature values:https://web3js.readthedocs.io/en/1.0/web3-eth-accounts.html#signtransaction
 */
  async signTransaction (tx, privateKey) {
    return await this.web3.eth.accounts.signTransaction(tx, privateKey)
  }

  /**
   * Publish a signed transaction to the network.
   *
   * @async
   * @function sendSignedTransaction
   * @param {object} transaction The transaction to be broadcast.
   * @returns {Promise} A PromiEvent which will be resolved when the transaction receipt is available
   */
  async sendSignedTransaction (transaction) {
    return await new Promise((resolve, reject) => {
      this.web3.eth.sendSignedTransaction('0x' + transaction.serialize().toString('hex'))
        .once('receipt', receipt => resolve(receipt))
        .on('error', err => reject(err))
    })
  }

  /**
   * Create and publish a transaction to the network.
   *
   * @async
   * @function sendTransaction
   * @param {object} tx The transaction object to send.
   * @param {string} tx.from The Address the tokens are being sent from.
   * @param {string} tx.to (optional) The destination address of the message, leave undefined for a contract-creation transaction.
   * @param {string} tx.value (optional) The value of wei to be transferred.
   * @param {string} tx.gas (optional) The amount of gas to use for the transaction (unused gas is refunded), default: To-Be-Determined.
   * @param {string} tx.gasPrice (optional) The price of gas for this transaction in wei, defaults to web3.eth.gasPrice.
   * @param {object|JSON} tx.data (optional )Either a ABI byte string containing the data of the function call on a contract, or contract code for deployment.
   * @param {string} tx.nonce (optional) Integer of a nonce. This allows to overwrite your own pending transactions that use the same nonce.
   * @returns {string} A PromiEvent which will be resolved when the transaction receipt is available
   */
  async sendTransaction (tx) {
    return await new Promise((resolve, reject) => {
      this.web3.eth.sendTransaction({ tx })
        .once('receipt', receipt => resolve(receipt))
        .on('error', err => reject(err))
    })
  }

  /**
   * Get the transaction receipt of the given transaction hash.
   *
   * @async
   * @function getTransactionReceipt
   * @param {string} hash The transaction hash.
   * @returns {object} The receipt or null if a receipt does not exist.
   */
  async getTransactionReceipt (hash) {
    return this.web3.eth.getTransactionReceipt(`0x${ hash }`)
  }
}

/**
 * exports
 * @ignore
 */
module.exports = Transaction
