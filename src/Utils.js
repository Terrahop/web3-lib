'use strict'

const units = [null, 'wei', null, 'kwei', null, null, 'mwei', null, null, 'gwei', null, null,
  'szabo', null, null, 'finney', null, null, 'ether', null, null, 'grand', null, null, 'mether',
  null, null, 'gether', null, null, 'tether',
]

/**
 * Utils
 * Provides utility functions for Ethereum dapps and other web3.js packages.
 * @class
 */

class Utils {
  constructor (RPCInstance) {
    Object.defineProperty(this, 'web3', { value: RPCInstance, writable: true })
  }

  /**
   * Checks if the provided address is a valid account address.
   * @param {string} address The address to check.
   * @returns {boolean} Whether the address is valid.
   */
  async isAddress (address) {
    return this.web3.utils.isAddress(address)
  }

  /**
   * The randomHex library to generate cryptographically strong pseudo-random HEX strings from a given byte size.
   * @param {number} size The byte size for the HEX string.
   * @returns {string} The generated random HEX string.
 */
  async randomHex (size) {
    return this.web3.utils.randomHex(size)
  }

  /**
   * Converts given value to HEX.
   * @param {string|number} value The input to convert to HEX.
   * @returns {string} The resulting HEX string.
   */
  async toHex (value) {
    return this.web3.utils.toHex(value)
  }
  /**
   * Will safly convert any given value into a BN.js instance, for handling big numbers in JavaScript.
   * @param {string|number} number Number to convert to a big number.
   * @returns {object} The BN.js instance.
   */
  async toBN (number) {
    return this.web.utils.toBN(number)
  }

  /**
   * Returns the number representation of a given HEX value as a string.
   * @param {string} hexString A string to hash.
   * @returns {string} The number as a string.
   */
  async hexToNumberString (hexString) {
    return this.web3.utils.hexToNumberString(hexString)
  }

  /**
   * Returns the balance value with the correct decimal placing for display.
   * @param {string} value The uncorrected value
   * @returns {number} The corrected value for display
   */
  async correctDecimals (value, decimals = 18) {
    const unit = units[decimals] || 'wei'
    return this.web3.utils.fromWei(value, unit)
  }

  /**
   * Returns the correct amount value for transacting.
   * @param {string} value The float value
   * @returns {number} The corrected value for transacting
   */
  async calculateAmount (value, decimals = 18) {
    const unit = units[decimals] || 'wei'
    return this.web3.utils.toWei(value, unit)
  }
}

/**
 * exports
 * @ignore
 */
module.exports = Utils
