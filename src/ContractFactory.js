'use strict'

/**
 * imports
 * @ignore
 */
const Contract = require('./Contract').default
const fs = require('fs')
const solc = require('solc')

/**
 * Contract Factory
 * Used to create multiple contract instances
 * @class
 */
class ContractFactory {
  constructor (RPCInstance) {
    Object.defineProperty(this, 'web3', { value: RPCInstance })
  }

  /**
   * Create a web3 contract instance from a given solidity file for deployment or for
   * interacting with an already deployed contract
   *
   * @function createContractFromSol
   * @param {string} ownerAddress Address of the owner
   * @param {string} contractSolPath  Path to the contract sol file
   * @param {string} contractClassName The name of the class in the contract that contains logic.
   * @param {string} deployedAddress (optional) The address where the contract is deployed
   * @returns A new Contract class initiated with given contract data
   */
  createContractFromSol (ownerAddress, contractSolPath, contractClassName, deployedAddress) {
    const input = fs.readFileSync(contractSolPath)
    const output = solc.compile(input.toString(), 1)
    const contractByteCode = output.contracts[`:${contractClassName}`].bytecode
    const contractAbi = JSON.parse(output.contracts[`:${contractClassName}`].interface)
    let contractInstance = new this.web3.eth.Contract(contractAbi, deployedAddress)
    return new Contract(this.web3, {
      ownerAddress,
      contractInstance,
      contractByteCode,
      contractAbi,
      deployedAddress
    })
  }

  /**
   * Create a web3 contract instance from given byte code and JSON abi for deployment
   * or for interacting with an already deployed contract
   *
   * @function createContract
   * @param {string} ownerAddress Address of the owner
   * @param {string} contractByteCode The byte code of the contract
   * @param {JSON | string} contractAbiJSON The JSON of the contract Abi
   * @param {string} deployedAddress (optional) The address where the contract is deployed
   * @returns A new Contract class initiated with given contract data
   */
  createContract (ownerAddress, contractByteCode, contractAbiJSON, deployedAddress) {
    let contractAbi = JSON.parse(contractAbiJSON)
    let contractInstance = new this.web3.eth.Contract(contractAbi, deployedAddress)
    return new Contract(this.web3, {
      ownerAddress,
      contractInstance,
      contractByteCode,
      contractAbi,
      deployedAddress
    })
  }
}

/**
 * exports
 * @ignore
 */
module.exports = ContractFactory
