'use strict'

/**
 * imports
 * @ignore
 */
const Web3 = require('web3')
const ContractFactory = require('./ContractFactory')
const Account = require('./Account')
const Utils = require('./Utils')
const Transaction = require('./Transaction')

/**
 * Server class that set's up the initial connection to given provider or defaults to
 * infura's rinkeby network
 * @class
 */
class Server {
  constructor (options = {}) {
    const { Provider = new Web3.providers.HttpProvider('https://rinkeby.infura.io') } = options
    Object.defineProperty(this, 'host', { value: Provider })
    const web3 = new Web3()
    Object.defineProperty(this, 'web3', { value: web3 })
    web3.setProvider(this.host)
  }

  /**
   * Initialize a new Contract Factory instance
   *
   * @function newContractFactory
   * @returns {ContractFactory} New Contract Factory for creating contracts
   */
  newContractFactory () {
    return new ContractFactory(this.web3)
  }

  /**
   * Initialize a new Account instance
   *
   * @function newAccount
   * @returns {Account} New Account class instance
   */
  newAccount () {
    return new Account(this.web3)
  }

  /**
   * Initialize a new Utils instance
   *
   * @function newUtils
   * @returns {Utils} New Utils class instance
   */
  newUtils () {
    return new Utils(this.web3)
  }

  /**
   * Initialize a new Transaction instance
   *
   * @function newTransaction
   * @returns {Transaction} New Transaction class instance
   */
  newTransaction () {
    return new Transaction(this.web3)
  }
}

/**
 * exports
 * @ignore
 */
module.exports = Server
