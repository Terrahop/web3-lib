'use strict'

/**
 * Account
 * Contains functions to create and interact with accounts
 * @class
 */
class Account {
  constructor (RPCInstance) {
    Object.defineProperty(this, 'web3', { value: RPCInstance, writable: true })
  }

  /**
  * Unlocks an account address with a password
  *
  * @function unlockAccount
  * @param {string} address Address of the account on the network
  * @param {string} password Password for given account
  * @param {number} duration (optional) Duration the account should be unlocked for, defaults to 5 minutes
  * @returns {boolean} True if the account unlocks successfully
  */
  unlockAccount (address, password, duration = '300') {
    return this.web3 = this.web3.eth.personal.unlockAccount(address, password, duration)
  }

  /**
   * Creates a new account with the given password
   *
   * @async
   * @function createAccountWithPassword
   * @param {string} password Password to be associated with the account
   * @returns {string} Address of the generated account if successfull
   */
  async createAccountWithPassword (password) {
    return new this.web3.eth.personal.newAccount(password)
  }

  /**
   * Generates a new account from the given entropy that contains a private key
   *
   * @async
   * @function createAccount
   * @param {string} entropy Optional random string for custom entropy, should be at least 32 characters.
   * @returns {object} An object containing the address, privateKey, a signTransaction() and sign() function
   */
  async createAccount (entropy = '') {
    return this.web3.eth.accounts.create(entropy)
  }

  /**
   * Creates a new account from a given private key.
   *
   * @async
   * @function createAccountFromPrivateKey
   * @param {string} privateKey The private key to generate the account from.
   * @returns {object} The account object: https://web3js.readthedocs.io/en/1.0/web3-eth-accounts.html#eth-accounts-create-return
   */
  async createAccountFromPrivateKey (privateKey) {
    return this.web3.eth.accounts.privateKeyToAccount('0x' + privateKey)
  }

  /**
   * Checks the balance of an account
   *
   * @async
   * @function checkBalance
   * @param {string} accountAddress Address of the account to check
   * @returns {string} Balance of account in wei
   */
  async checkBalance (accountAddress) {
    return this.web3.eth.getBalance(accountAddress)
  }
}

/**
 * exports
 * @ignore
 */
module.exports = Account
