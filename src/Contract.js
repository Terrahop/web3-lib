'use strict'

/**
 * imports
 * @ignore
 */
const Tx = require('ethereumjs-tx')

/**
 * Contract
 * Allows for interactioning with smart contracts on the ethereum blockchain.
 * @class
 */
class Contract {
  constructor (RPCInstance, { ownerAddress, contractInstance, contractByteCode, contractAbi, deployedAddress }) {
    Object.defineProperty(this, 'web3', { value: RPCInstance })
    Object.defineProperty(this, 'ownerAddress', { value: ownerAddress })
    Object.defineProperty(this, 'contractInstance', { value: contractInstance, writable: true })
    Object.defineProperty(this, 'contractByteCode', { value: contractByteCode, writable: true })
    Object.defineProperty(this, 'contractAbi', { value: contractAbi, writable: true })
    Object.defineProperty(this, 'deployedAddress', { value: deployedAddress, writable: true })
  }

  /**
   * Deploy a contract to the network from a password unlocked account.
   *
   * @async
   * @function deployContractFromUnlockedAccount
   * @param {array} contractArguments (Optional) An array of arguments(if any) to be supplied to the contract constructor.
   * @param {string} gas (Optional) The maximum gas provided for a transaction (gas limit), Defaults to a gas estimate.
   * @param {string} gasPrice (Optional) The gas price in wei converted to hex to use for transactions.
   * @returns {object} The deployed contract object instance.
   */
  async deployContractFromUnlockedAccount (contractArguments, gas, gasPrice) {
    if (this.deployedAddress) {
      throw new Error('You are trying to deploy an already deployed contract')
    }

    const gasEstimate = await this.contractInstance.deploy({
      data: '0x' + this.contractByteCode,
      arguments: contractArguments
    }).estimateGas()

    const gasPriceHex = await this.web3.toHex(this.web3.eth.getGasPrice())

    const deployedContract = await this.contractInstance.deploy({
      data: '0x' + this.contractByteCode,
      arguments: contractArguments
    }).send({
      from: this.ownerAddress,
      gas: gas ? gas : gasEstimate,
      gasPrice: gasPrice ? gasPrice : gasPriceHex
    })

    this.deployedAddress = deployedContract.options.address
    this.contractInstance = new this.web3.eth.Contract(this.contractAbi, deployedContract.options.address)

    return deployedContract
  }

  /**
   * Deploy a contract to the network using the owner account's private key.
   *
   * @async
   * @function deployContractWithPrivateKey
   * @param {string} privateKey The private key of the owner's account.
   * @param {array} contractArguments (Optional) The array of arguments required by the contracts constructor.
   * @param {string} gas (Optional) The maximum gas provided for a transaction (gas limit), Defaults to a gas estimate.
   * @param {string} gasPrice (Optional) The gas price in wei converted to hex to use for transactions.
   * @returns The deployed contract object instance.
   */
  async deployContractWithPrivateKey (privateKey, contractArguments, gas, gasPrice) {
    if (this.deployedAddress) {
      throw new Error('You are trying to deploy an already deployed contract')
    }

    const privKey = new Buffer(privateKey, 'hex')
    const gasEstimate = await this.contractInstance.deploy({
      data: '0x' + this.contractByteCode,
      arguments: contractArguments
    }).estimateGas()
    const gasPriceHex = await this.web3.toHex(this.web3.eth.getGasPrice())

    const nounce = await this.web3.toHex(this.web3.eth.getTransactionCount(this.ownerAddress))
    const deploy = await this.contractInstance.deploy({ data: this.contractByteCode }).encodeABI()

    const rawTx = {
      nonce: nounce,
      gas: gas ? gas : gasEstimate,
      gasPrice: gasPrice ? gasPrice : gasPriceHex,
      data: deploy,
      from: this.ownerAddress
    }
    const signedTx = await this.web3.eth.accounts.signTransaction(rawTx, privKey)
    const deployedContract = await this.web3.eth.sendSignedTransaction(signedTx.rawTransaction)
      .on('reciept', (reciept) => { this.deployedAddress = reciept.contractAddress })

    this.contractInstance = new this.web3.eth.Contract(this.contractAbi, this.deployedAddress)

    return deployedContract
  }

  /**
   * Execute methods on a deployed contract and get returned values.
   *
   * @async
   * @function methodCall
   * @param {string} methodName Name of the method on the contract.
   * @param {array} params (Optional) Any parameters the method takes.
   * @returns {any | object} The return value(s) of the smart contract method.
   */
  async methodCall (methodName, params = []) {
    if (!this.deployedAddress) {
      throw new Error('No deployed address supplied for the contract')
    }

    return await this.contractInstance.methods[methodName](...params)
      .call({ from: this.ownerAddress })
  }

  /**
   * Execute methods on a deployed contract.
   *
   * @async
   * @function methodSend
   * @param {string} methodName Name of the method on the contract.
   * @param {array} params (Optional) Any parameters the method takes.
   * @returns {any | object} The return value(s) of the smart contract method.
   */
  async methodSend (methodName, params = []) {
    if (!this.deployedAddress) {
      throw new Error('No deployed address supplied for the contract')
    }

    return await this.contractInstance.methods[methodName](...params)
      .send({ from: this.ownerAddress })
  }

  /**
   * Estimate the gas used to run a method on a deployed contract.
   *
   * @async
   * @function estimateGas
   * @param {string} methodName Name of the method on the contract.
   * @param {array} params Any parameters the method takes.
   * @returns {number} The gas amount estimated.
   */
  async estimateGas (methodName, params = []) {
    return await this.contractInstance.methods[methodName](...params).estimateGas({ from: this.ownerAddress })
  }

  /**
   * Get either all the contract's past events or events specified.
   *
   * @async
   * @function getPastEvents
   * @param {string} event The name of the event in the contract, defaults to 'allEvents'.
   * @param {object} options The additional options for specifying which past events to get.
   * @param {object} options.filter (optional) Lets you filter events by indexed parameters.
   * @param {number} options.fromBlock (optional) The block number from which to get events on.
   * @param {number} options.toBlock optional) The block number to get events up to (Defaults to "latest").
   * @param {array} options.topics (optional) This allows manually setting the topics for the event filter. If given the filter property and event signature.
   * @returns {array} An array with the past event Objects, matching the given event name and filter.
   */
  async getPastEvents (event = 'allEvents', { options }) {
    return await this.contractInstance.getPastEvents(event, { options })
  }

  /**
   * Create a transaction to transfer ERC20 Tokens, this requires there be a 'transfer' method for ERC20 Tokens in the contract
   *
   * @async
   * @function createTransferTransaction
   * @param {string} gasPrice The gas price in wei to use for transactions.
   * @param {string} gasLimit The gas limit
   * @param {string} fromAddress The address of the token sender
   * @param {string} toAddress The address of the token receiver
   * @param {string} amount Amount of tokens to transfer
   * @param {string} privateKey Private key of account that's sending tokens
   * @param {number} nonceDiscriminator (optional) A counter to be added to the transaction nonce
   * @returns {object} The transaction object ready to be published
   */
  async createTransferTransaction (
    gasPrice,
    gasLimit,
    fromAddress,
    toAddress,
    amount,
    privateKey,
    nonceDiscriminator = 0,
  ) {
    if (!this.deployedAddress) {
      throw new Error('No contract address set')
    }

    const count = await this.web3.eth.getTransactionCount(fromAddress)
    const privKey = Buffer.from(privateKey, 'hex')
    const rawTx = {
      from: fromAddress,
      gasPrice,
      gasLimit,
      to: this.deployedAddress,
      value: '0x0',
      data: this.contractInstance.methods.transfer(toAddress, amount).encodeABI(),
      nonce: this.web3.utils.toHex(count + nonceDiscriminator),
    }
    const transaction = new Tx(rawTx)
    transaction.sign(privKey)

    return transaction
  }
}

/**
 * exports
 * @ignore
 */
module.exports = Contract
